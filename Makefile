CFLAGS+=-std=c99
LDFLAGS+=-lm

ifdef DEBUG
	CFLAGS+=-ggdb -DDEBUG -O0 -Wall -pedantic
else
	CFLAGS+=-DNDEBUG -Werror
endif

all: shitsynth
