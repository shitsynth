#define _POSIX_C_SOURCE 200809L
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/soundcard.h>
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <error.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdbool.h>

/* Stupid declarations */

#define BIT_RATE 48000

typedef int16_t sample_t;
#define SAMPLE_MIN -32768
#define SAMPLE_MAX 32767

typedef void(*synth_t)(uint_least16_t freq, sample_t *buf, size_t count);

int F;

uint_least16_t note(int_least8_t X) {
	return 440 * pow(2, X / 12.0);
}

/* Synthesizers */

void void_synth(uint_least16_t freq, sample_t *buf, size_t count) {
	memset(buf, 0, count * sizeof(sample_t));
}

void square_synth(uint_least16_t freq, sample_t *buf, size_t count) {
	sample_t a = SAMPLE_MAX;
	uint_least16_t period = BIT_RATE / freq;

	for (size_t i = 0; i < count; i++) {
		if (!(i%period)) a = (a == SAMPLE_MAX) ? SAMPLE_MIN : SAMPLE_MAX;
		buf[i] = a;
	}
}

/* Signal manipulating tools */

void attenuate(sample_t *track, size_t len, double level) {
	for (size_t i = 0; i < len; i++) {
		track[i] *= level;
	}
}

void mix(sample_t *dest, sample_t *src, size_t len) {
	for (size_t i = 0; i < len; i++) {
		dest[i] += src[i];
	}
}

/* Misc stuff */

void play(sample_t *what, size_t len) {
	write(F, what, len * sizeof(sample_t));
}

void show(sample_t *what, size_t len) {
	for (size_t i = 0; i < len; i++) {
		printf("%hd ", what[i]);
	}
}

void oss_init() {
	int i;
	F = open("/dev/dsp", O_WRONLY);
	if (F == -1) {
		error(1, errno, NULL);
	}

	i = 1;
	ioctl(F, SNDCTL_DSP_CHANNELS, &i);
	i = AFMT_S16_NE;
	ioctl(F, SNDCTL_DSP_SETFMT, &i);
	i = BIT_RATE;
	ioctl(F, SNDCTL_DSP_SPEED, &i);
}

void interpret_track(char *instr, size_t instrlen, char *what, uint_least16_t *off, int_least16_t size) {
	char n = what[*off];
	assert(instr);
	sample_t *b1 = malloc(size * sizeof(sample_t));

	for (size_t len = strchr(what + *off, '\n') - what; *off < len; (*off)++) {
		if (n == ' ') {
			void_synth(0, b1, size);
		} else {
			if (strncmp(instr, "square", labellen) == 0) {
				square_synth(note((what[off] - 'e') * 2), b1, size);
			} else {
				assert(false);
				/*
				char *fail;
				uint_least8_t track = strtol(label, &fail, 10);
				assert(fail != label);
				*/
				//TODO
			}
		}
		play(b1, size);
	}
}

void interpret(char *what, size_t len, int_least16_t size) {
	uint_least16_t off = 0;
	sample_t *b1 = malloc(size * sizeof(sample_t));

	assert(what);
	assert(size);
	//if (size > ) { /* size is per note */
	//} else { /* size is per code chunk */
	//}

	char *label = NULL;
	while (off < len) {
		char n = what[off];
		putchar(n);
		putchar('\n');
		fdatasync(1);

		size_t labellen;

		if (off == 0 || what[off - 1] == '\n') {
			if (n == '>') {	// define the subroutine
				//TODO
			} else if (n == '<') { // use the subroutine
				label = what + off + 1;
				labellen = strchr(label, '>') - label;
				off += labellen + 1;
			} else {
				assert(false);
			}

		} else {
		}
		off++;
	}
}

int main() {
	int i;
	int songf = open("song", O_RDONLY);
	size_t songlen;
	char *song, *songfile;
	//size_t chunk = BIT_RATE * 0.04;
	sample_t *b1;

	assert(songf != -1);

	songlen = lseek(songf, 0, SEEK_END);
	assert(songlen);
	lseek(songf, 0, SEEK_SET);
	songfile = mmap(NULL, songlen, PROT_READ, MAP_SHARED, songf, 0);
	assert(songfile != MAP_FAILED);

	double notelength;
	sscanf(songfile, "%lf", &notelength);
	printf("Notelength: %lf\n", notelength);
	assert(notelength > 0);
	size_t notelen = notelength * BIT_RATE; // :]

	song = strchr(songfile, '\n') + 1;

	oss_init();

	b1 = malloc(notelen * sizeof(sample_t));

//	show(b1, chunk);
	off_t songptr = 0;
	char *programs[16] = {0};

	interpret(song, songlen, notelen);

	return 0;
}
